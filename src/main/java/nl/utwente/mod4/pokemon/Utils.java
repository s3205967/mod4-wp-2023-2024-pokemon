package nl.utwente.mod4.pokemon;

import java.nio.file.Path;
import java.util.List;

public class Utils {

    public static String getAbsolutePathToResources() {
        return "C:\\Users\\steef\\Documents\\Vaults\\Brain\\Uni\\Year 1\\Module 4\\Data & Information\\WP-lab\\PokeApp\\src\\main\\resources";
    }

    public static <T extends Comparable<T>> int compare(T o1, T o2) {
        if (o1 == null && o2 == null)
            return 0;
        if (o1 == null)
            return -1;
        if (o2 == null)
            return 1;
        return o1.compareTo(o2);
    }

    public static List<?> pageSlice(List<?> list, int pageSize, int pageNumber) {
        int total = list.size();
        int firstIndex = (pageNumber - 1) * pageSize;
        int lastIndex = Math.min(pageNumber * pageSize, total);
        return list.subList(firstIndex,lastIndex);
    }

}
